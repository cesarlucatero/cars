﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
GO
SET IDENTITY_INSERT [dbo].[Make] ON 
GO
INSERT [dbo].[Make] ([MakeId], [MakeName], [MakeCountry]) VALUES (1, N'Porsche', N'Germany')
GO
INSERT [dbo].[Make] ([MakeId], [MakeName], [MakeCountry]) VALUES (2, N'Ferrari', N'Italy')
GO
INSERT [dbo].[Make] ([MakeId], [MakeName], [MakeCountry]) VALUES (3, N'Aston Martin', N'UK')
GO
INSERT [dbo].[Make] ([MakeId], [MakeName], [MakeCountry]) VALUES (4, N'Ford', N'U.S.A.')
GO
SET IDENTITY_INSERT [dbo].[Make] OFF
GO
SET IDENTITY_INSERT [dbo].[Model] ON 
GO
INSERT [dbo].[Model] ([ModelId], [MakeId], [Name], [Year]) VALUES (3, 1, N'Cayman', N'2018')
GO
INSERT [dbo].[Model] ([ModelId], [MakeId], [Name], [Year]) VALUES (4, 2, N'LeFerrari', N'2019')
GO
INSERT [dbo].[Model] ([ModelId], [MakeId], [Name], [Year]) VALUES (5, 3, N'Vanquish', N'2017')
GO
INSERT [dbo].[Model] ([ModelId], [MakeId], [Name], [Year]) VALUES (6, 4, N'Raptor', N'2011')
GO
SET IDENTITY_INSERT [dbo].[Model] OFF
GO
SET IDENTITY_INSERT [dbo].[Trim] ON 
GO
INSERT [dbo].[Trim] ([TrimId], [ModelId], [HasLeather], [HasSunroof], [Name]) VALUES (4, 3, 1, 0, N'S')
GO
INSERT [dbo].[Trim] ([TrimId], [ModelId], [HasLeather], [HasSunroof], [Name]) VALUES (6, 6, 1, 1, N'SVT')
GO
SET IDENTITY_INSERT [dbo].[Trim] OFF
GO