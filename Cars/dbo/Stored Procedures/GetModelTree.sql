﻿

CREATE PROCEDURE [dbo].[GetModelTree] @makeId INT = -1
AS
    SELECT [dbo].[Make].[MakeName] AS [Make]
			,[dbo].[Model].[Name] AS [Model]
			,[dbo].[Trim].[Name] AS [Trim]
    FROM   dbo.[Make]
			JOIN [dbo].[Model]
				ON [dbo].[Make].[MakeId] = [dbo].[Model].[MakeId]
			LEFT JOIN [dbo].[Trim]
				ON [dbo].[Model].[ModelId] = [dbo].[Trim].[ModelId]
	WHERE [dbo].[Make].[MakeId] = @makeId

    RETURN 0