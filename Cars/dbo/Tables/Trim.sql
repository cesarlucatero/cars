﻿CREATE TABLE [dbo].[Trim]
(
	[TrimId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ModelId] INT NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL,
    [HasLeather] BIT NOT NULL, 
    [HasSunroof] BIT NOT NULL
)
GO
ALTER TABLE [dbo].[Trim]
ADD CONSTRAINT [FK_Trim_Model] FOREIGN KEY ([ModelId]) REFERENCES [dbo].[Model]([ModelId])