﻿CREATE TABLE [dbo].[Model]
(
	[ModelId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [MakeId] INT NOT NULL,
    [Name] NVARCHAR(50) NOT NULL, 
    [Year] NVARCHAR(50) NOT NULL
)
GO
ALTER TABLE [dbo].[Model]
ADD CONSTRAINT [FK_Model_Make] FOREIGN KEY ([MakeId]) REFERENCES [dbo].[Make] ([MakeId])

